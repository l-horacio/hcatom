#ifndef _ATOM_H_
#define _ATOM_H_

enum atom_type {
    ATOM_NIL,
    ATOM_BOOL,
    ATOM_INTERGER,
    ATOM_FLOAT,
    ATOM_LIST,
    ATOM_STRING,
};

struct atom_list;

struct atom {
    enum atom_type type;

    union {
        long integer;
        double real;
        char *string;
        char boolean;
        struct atom_list *list;
    } value;
};

struct atom_list {
    struct atom value;
    struct atom_list *next;
};

struct atom_list *make_list(struct atom);
struct atom_list *list_append(struct atom_list*, struct atom);
void list_free(struct atom_list*);
void list_remove(struct atom_list*, struct atom);
void list_remove_index(struct atom_list*, unsigned long);
int list_cmp(struct atom_list *, struct atom_list *);

struct atom make_atom_integer(const long);
struct atom make_atom_float(const double);
struct atom make_atom_string(const char *, size_t);
struct atom make_atom_bool(const char);
struct atom make_atom_list(void);
struct atom make_atom_nil(void);

void atom_free(struct atom);

int atom_cmp(struct atom, struct atom);

#ifdef ATOM_IMPL
#include <stdlib.h>
#include <string.h>

struct atom make_atom_integer(const long v)
{
    return (struct atom) {
        .type = ATOM_INTERGER,
        .value.integer = v
    };
}

struct atom make_atom_float(const double v)
{
    return (struct atom) {
        .type = ATOM_FLOAT,
        .value.real = v
    };
}

struct atom make_atom_string(const char *c, size_t s)
{
    struct atom r = (struct atom) {
        .type = ATOM_STRING,
        .value.string = malloc(s + 1)
    };

    memcpy(r.value.string, c, s);
    r.value.string[s] = 0;

    return r;
}

struct atom make_atom_bool(const char v)
{
    return (struct atom) {
        .type = ATOM_BOOL,
        .value.boolean = v
    };
}

struct atom make_atom_list(void)
{
    return (struct atom) {
        .type = ATOM_LIST,
        .value.list = 0
    };
}

struct atom make_atom_nil(void)
{
    return (struct atom) {
        .type = ATOM_NIL,
        .value.integer = 0
    };
}

void atom_free(struct atom atom)
{
    switch(atom.type)
    {
    case ATOM_LIST:
        list_free(atom.value.list);
        break;
    case ATOM_STRING:
        free(atom.value.string);
        break;
    }
}

int atom_cmp(struct atom lhs, struct atom rhs)
{
    if(lhs.type != rhs.type)
        return 0;

    switch(lhs.type) {
    case ATOM_NIL:
        return 1;
    case ATOM_BOOL:
        return lhs.value.boolean == rhs.value.boolean;
    case ATOM_INTERGER:
        return lhs.value.integer == rhs.value.integer;
    case ATOM_FLOAT:
        return lhs.value.real == rhs.value.real;
    case ATOM_LIST:
        return list_cmp(lhs.value.list, rhs.value.list);
    case ATOM_STRING:
        return strcmp(lhs.value.string, rhs.value.string) == 0;
    }
}

struct atom_list *make_list(struct atom v)
{
    struct atom_list *r = malloc(sizeof(struct atom_list));
    r->value = v;
    r->next = 0;
    return r;
}

struct atom_list *list_append(struct atom_list *l, struct atom v)
{
    if(l == 0)
        return make_list(v);

    struct atom_list *curr = l;

    while(curr->next != 0)
        curr = curr->next;

    curr->next = make_list(v);

    return l;
}

void list_free(struct atom_list* l)
{
    struct atom_list *curr;
    while(l != 0) {
        curr = l;
        atom_free(curr->value);
        l = l->next;
        free(curr);
    }
}

void list_remove(struct atom_list *l, struct atom v)
{
    struct atom_list *prev = l;
    while(l != 0 && atom_cmp(l->value, v)) {
        prev = l;
        l = l->next;
    }

    if(l == 0)
        return;

    prev->next = l->next;
    l->next = 0;
    list_free(l);
}

void list_remove_index(struct atom_list *l, unsigned long i)
{
    unsigned long j = 0;
    struct atom_list *prev = l;
    while(l != 0 && i != j) {
        ++j;
        prev = l;
        l = l->next;
    }

    if(l == 0)
        return;

    prev->next = l->next;
    l->next = 0;
    list_free(l);
}

int list_cmp(struct atom_list *lhs, struct atom_list *rhs)
{
    if(lhs == 0 || rhs == 0) {
        return rhs == lhs;
    }

    if(!atom_cmp(lhs->value, rhs->value))
        return 0;

    return list_cmp(lhs->next, rhs->next);
}
#endif //ATOM_IMPL

#endif //_ATOM_H_
